# Reaction Time Game

This is the code repo for a youtube video project you can find [here](https://youtu.be/6yk8SgossMQ).

The circuit for this project is shown below

![Circuit](reaction_circuit.png)